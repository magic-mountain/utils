package concurrent

import (
	"container/list"
	"sync"
)

type ConcurrentList struct {
	list  *list.List
	mutex sync.Mutex
}

func New() *ConcurrentList {
	return &ConcurrentList{list: list.New()}
}

func (c *ConcurrentList) Front() *list.Element {
	return c.list.Front()
}
func (c *ConcurrentList) Back() *list.Element {
	return c.list.Back()
}

func (c *ConcurrentList) MoveToFront(e *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.MoveToFront(e)
}

func (c *ConcurrentList) MoveToBack(e *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.MoveToBack(e)
}
func (c *ConcurrentList) MoveAfter(e *list.Element, mark *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.MoveAfter(e, mark)
}

func (c *ConcurrentList) InsertAfter(v interface{}, mark *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.InsertAfter(v, mark)
}

func (c *ConcurrentList) InsertBefore(v interface{}, mark *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.InsertBefore(v, mark)
}

func (c *ConcurrentList) PushFront(v interface{}) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.PushFront(v)
}

func (c *ConcurrentList) Remove(e *list.Element) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.list.Remove(e)
}
