package median

import (
	"fmt"
	"math/rand"
	"strconv"
	"testing"
	"time"
)

func Test_m(t *testing.T) {

	rand.Seed(time.Now().UnixNano())
	n, k := 10, 2
	a := make([]int, n)
	for i := 0; i < n; i++ {
		a[i] = rand.Intn(1000)
		fmt.Printf("%d ", a[i])
	}
	fmt.Println()
	fmt.Printf("The %d th number is : %d\n", k, RandomSelect(a, 0, n-1, k))

	for i := 0; i < n; i++ {
		fmt.Printf("%d ", a[i])
	}
	fmt.Println()
}

func Test_b(t *testing.T) {

	rand.Seed(time.Now().UnixNano())
	n, k := 10, 2
	a := make([][]byte, n)
	a[0] = []byte(strconv.Itoa(2))
	for i := 1; i < n; i++ {
		a[i] = []byte(strconv.Itoa(rand.Intn(1000)))
		fmt.Printf("%s ", a[i])
	}
	fmt.Println()
	fmt.Printf("The %d th number is : %s\n", k, RandomSelectb(a, 0, n-1, k))

	for i := 0; i < n; i++ {
		fmt.Printf("%s ", a[i])
	}
	fmt.Println()
}
