package utils

import (
	"reflect"
	"unsafe"
)

func Pointer2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
func Pointer2Bytes(s string) []byte {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := reflect.SliceHeader{
		Data: sh.Data,
		Len:  sh.Len,
		Cap:  sh.Len,
	}
	return *(*[]byte)(unsafe.Pointer(&bh))
}
