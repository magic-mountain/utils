module gitee.com/magic-mountain/utils

go 1.16

require (
	github.com/filecoin-project/go-jsonrpc v0.1.5
	github.com/gbrlsnchs/jwt/v3 v3.0.1
	github.com/ipfs/go-log/v2 v2.4.0
	github.com/urfave/cli v1.22.5
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
)
