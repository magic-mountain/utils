package rpc

import "time"

type RpcService struct {
	Url           string
	V             interface{}
	Closer        func()
	Reaction_time time.Duration
}
